package com.doublefish.export

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter

class MainActivity : AppCompatActivity() {

    private val CSV_HEADER = "id,name,class,skips"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            val csvFile = File(filesDir.path, "export.csv")
            val fileOut = OutputStreamWriter(FileOutputStream(csvFile), "UTF-8")
            fileOut.write(String(byteArrayOf(0xEF.toByte(), 0xBB.toByte(), 0xBF.toByte())))
            var fileWriter: BufferedWriter? = null

            try {
                fileWriter = BufferedWriter(fileOut)

                fileWriter.append(CSV_HEADER)
                fileWriter.append('\n')


                fileWriter.append("123")
                fileWriter.append(',')
                fileWriter.append("Тест русский язык")
                fileWriter.append(',')
                fileWriter.append("11-1")
                fileWriter.append(',')
                fileWriter.append("30")
                fileWriter.append('\n')
                fileWriter.flush()
                fileWriter.close()

                println("Success")

            } catch (e: Exception) {
                println("Can't write CSV!")
                e.printStackTrace()
            }


            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_STREAM, getFileUri())

            sendIntent.type = "text/csv"
            startActivity(Intent.createChooser(sendIntent, "Send CSV to..."))
        }
    }

    fun getFileUri(): Uri {
        val file = File(filesDir, "export.csv")
        return FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file)
    }
}
